#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

void main(int argc, char ** argv)
{
    int fd_brightness[4];
    int fd_trigger[4];
    char *file_trigger[4] =  {"/sys/class/leds/beaglebone:green:heartbeat/trigger","/sys/class/leds/beaglebone:green:mmc0/trigger","/sys/class/leds/beaglebone:green:usr2/trigger","/sys/class/leds/beaglebone:green:usr3/trigger"};;
    char *file_brightness[4] =  {"/sys/class/leds/beaglebone:green:heartbeat/brightness","/sys/class/leds/beaglebone:green:mmc0/brightness","/sys/class/leds/beaglebone:green:usr2/brightness","/sys/class/leds/beaglebone:green:usr3/brightness"};
    char *none = "none";
    int i = 0;
    int time = 1000;
    int count2 = 0, count = 0;

    if(argc != 2)
    {
        printf("utilisation %s temps", argv[0]);
        exit(EXIT_FAILURE);
    }
    time = atoi(argv[1]);
    //initialisation
    for (i = 0; i<4; i++)
    {
        fd_brightness[i] = open(file_brightness[i], O_SYNC|O_WRONLY);

        if (fd_brightness[i] == -1 )
        {
            printf("file descriptor brightness %d did not open\n", i);
            exit(EXIT_FAILURE);
        }

        fd_trigger[i] = open(file_trigger[i], O_SYNC|O_WRONLY);
        if (fd_trigger[i] == -1 )
        {
            printf("file descriptor trigger %d did not open\n", i);
            exit(EXIT_FAILURE);
        }

        printf("put 0 in %s\n", file_brightness[i]);
        write(fd_brightness[i], "0", sizeof(char));
        write(fd_trigger[i], none, sizeof(char)*5);

        close(fd_trigger[i]);    
    }

    while (1)
    {
        switch(count){
            case 0:
            case 5 | 0:
                printf("Cas 0 ou 5");
                for(i = 0; i<4; i++)
                {
                    write(fd_brightness[i], "0", sizeof(char));
                }
                break;
            case 1:
            case 9:
                write(fd_brightness[0], "1", sizeof(char));
                write(fd_brightness[1], "0", sizeof(char));
                write(fd_brightness[2], "0", sizeof(char));
                write(fd_brightness[3], "0", sizeof(char));
                break;
            case 2:
            case 8:
                write(fd_brightness[0], "0", sizeof(char));
                write(fd_brightness[1], "1", sizeof(char));
                write(fd_brightness[2], "0", sizeof(char));
                write(fd_brightness[3], "0", sizeof(char));
                break;
            case 3:
            case 7:
                write(fd_brightness[0], "0", sizeof(char));
                write(fd_brightness[1], "0", sizeof(char));
                write(fd_brightness[2], "1", sizeof(char));
                write(fd_brightness[3], "0", sizeof(char));
                break;
            case 4:
            case 6:
                write(fd_brightness[0], "0", sizeof(char));
                write(fd_brightness[1], "0", sizeof(char));
                write(fd_brightness[2], "0", sizeof(char));
                write(fd_brightness[3], "1", sizeof(char));
                break;
            
        }
        usleep(time);
        for(i = 0; i<4; i++)
        {
            write(fd_brightness[i], "0", sizeof(char));
        }
        usleep(time);

        count2++;
        count = count2%10;

    }

    for (i = 0; i<4; i++)
    {
        close(fd_brightness[i]);
    }
  
    exit(EXIT_SUCCESS);
}