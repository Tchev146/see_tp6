#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <fcntl.h>
#include <signal.h>
#include <time.h>
#include <sys/wait.h>


#define PERIODE_500MS 500000000

int fd_brightness[4];
int count;

void changeStateLEDs(int cpt)
{
	if (cpt & 0b0001) {
		write(fd_brightness[0], "1", sizeof(char));
	}

	if (cpt & 0b0010) 
    {
		write(fd_brightness[1], "1", sizeof(char));
	}

	if (cpt & 0b0100) {
		write(fd_brightness[2], "1", sizeof(char));
	}

	if (cpt & 0b1000) {
		write(fd_brightness[3], "1", sizeof(char));
	}
}

void resetLEDs(void)
{
    int i;
	for (i = 0; i < 4; i++) {
		write(fd_brightness[i], "0", sizeof(char));
	}
}

void handler
(
  int sig, 
  siginfo_t *si,
  void *uc
)
{
    printf("Handler\n");
	count++;
    resetLEDs();
    changeStateLEDs(count%16);
}


void main(int argc, char **argv)
{
	pid_t pid;
    struct sigaction sa;
	timer_t timerid;

	int fd_trigger[4];
	char *file_trigger[4] = {
		"/sys/class/leds/beaglebone:green:heartbeat/trigger",
		"/sys/class/leds/beaglebone:green:mmc0/trigger",
		"/sys/class/leds/beaglebone:green:usr2/trigger",
		"/sys/class/leds/beaglebone:green:usr3/trigger"
	};
	;
	char *file_brightness[4] = {
		"/sys/class/leds/beaglebone:green:heartbeat/brightness",
		"/sys/class/leds/beaglebone:green:mmc0/brightness",
		"/sys/class/leds/beaglebone:green:usr2/brightness",
		"/sys/class/leds/beaglebone:green:usr3/brightness"
	};
	char *none = "none";
	int i = 0;
	int time = 1000;
	int count2 = 0;
    

    count = 0;

	/*
	if (argc != 2) {
		printf("utilisation %s temps", argv[0]);
		exit(EXIT_FAILURE);
	}
	time = atoi(argv[1]);
	*/

	pid = fork();
	if(pid == 0) // child - signal sender
	{
		/*struct sigevent sev;
		struct itimerspec its;
		int count_fils = 0;
		
		printf("Fils : Created\n");
		// creating timer
		sev.sigev_notify = SIGEV_SIGNAL;
		sev.sigev_signo = SIGUSR1;
		sev.sigev_value.sival_ptr = &timerid;
		
		if (timer_create(CLOCK_REALTIME, &sev, &timerid) == -1)
		{
			perror("timer_create");
			return;
		}

		printf("Fils : timer created\n");
		
		// starting timer
		its.it_value.tv_sec = 0;
		its.it_value.tv_nsec = PERIODE_500MS;
		its.it_interval.tv_sec = its.it_value.tv_sec;
		its.it_interval.tv_nsec = its.it_value.tv_nsec;
		
		if (timer_settime(timerid, 0, &its, NULL) == -1)
		{
			perror("timer_settime");
			return;
		}
		
		printf("Fils : timer started\n");
		*/

		sleep(1);
		int count_fils = 0;
		printf("PPID : %d\n", getppid());
		while(count_fils < 100)
		{
			count_fils++;
			kill(getppid(), SIGUSR1);
			sleep(1);
			if(!(count_fils%10))
			{
				printf("Fils : Rolling\n");
			}
		}
		printf("Fils : loop finished, ready to exit\n");
		exit(0);
	}
	else//parent - signal recieve
	{
		printf("Parent : Going on\n");
		// create signal
		sa.sa_flags = SA_SIGINFO;
		sa.sa_sigaction = handler;
		
		//initialisation
		for (i = 0; i < 4; i++) {
			fd_brightness[i] = open(file_brightness[i], O_SYNC | O_WRONLY);

			if (fd_brightness[i] == -1) {
				printf("file descriptor brightness %d did not open\n",
					i);
				exit(EXIT_FAILURE);
			}

			fd_trigger[i] = open(file_trigger[i], O_SYNC | O_WRONLY);
			if (fd_trigger[i] == -1) {
				printf("file descriptor trigger %d did not open\n", i);
				exit(EXIT_FAILURE);
			}

			printf("put 0 in %s\n", file_brightness[i]);
			write(fd_brightness[i], "0", sizeof(char));
			write(fd_trigger[i], none, sizeof(char) * 5);

			close(fd_trigger[i]);
		}

		if (sigaction(SIGUSR1, &sa, NULL) == -1)
		{
			perror("sigaction");
			return;
		}
		printf("Parent : ready to recieve\n");
		while(count2 < 100)
		{
			count2++;
			sleep(1);
			if(!(count%10))
			{
				printf("Parent : Rolling\n");
			}
		}
		printf("Parent : loop finished, ready to exit\n");
		

		for (i = 0; i < 4; i++) {
			close(fd_brightness[i]);
		}
		
	
		wait(NULL);
	}
	
	exit(EXIT_SUCCESS);
}
