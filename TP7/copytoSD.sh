#!/bin/bash

_path_to_SD="/run/media/kerboitk"
_path_to_output="/home/kerboitk/Documents/Cours/Master2/see/see_tp6/buildroot-2018.02.7/output/images"

if [ -e $_path_to_output ]
then 
	echo "Output path exist"
else 
	echo "Output path doesn't exist"
	exit 1
fi 


if [ -e "$_path_to_SD/boot" ]
then 
	echo "boot dir exist"
else 
	echo "boot dir doesn't exist"
	exit 1
fi 


if [ -e "$_path_to_SD/rootfs" ]
then 
	echo "rootfs dir exist"
else 
	echo "rootfs dir doesn't exist"
	exit 1
fi 


pushd "$_path_to_SD/boot/"
rm -r ./*
popd
pushd "$_path_to_SD/rootfs"
rm -r ./*
popd

pushd $_path_to_output 
cp MLO  "$_path_to_SD/boot"
cp u-boot.img  "$_path_to_SD/boot"
cp zImage  "$_path_to_SD/boot"
cp uEnv.txt "$_path_to_SD/boot"
cp am335x-boneblack.dtb  "$_path_to_SD/boot"

tar -C "$_path_to_SD/rootfs" -xf "$_path_to_output/rootfs.tar"
popd

umount "$_path_to_SD/boot"
umount "$_path_to_SD/rootfs"

exit 0
