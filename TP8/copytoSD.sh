#!/bin/bash

_path_to_SD="/run/media/kerboitk"
_path_to_output="/home/kerboitk/Documents/Cours/Master2/see/see_tp6/buildroot-2018.02.7/output/images"
_path_to_kernels="/home/kerboitk/Documents/Cours/Master2/see/see_tp6/TP8/"
_path_to_TP2="/home/kerboitk/Documents/Cours/Master2/see/TP2/"

if [ -e $_path_to_output ]
then 
	echo "Output path exist"
else 
	echo "Output path doesn't exist"
	exit 1
fi 


if [ -e "$_path_to_SD/boot" ]
then 
	echo "boot dir exist"
else 
	echo "boot dir doesn't exist"
	exit 1
fi 


if [ -e "$_path_to_SD/rootfs" ]
then 
	echo "rootfs dir exist"
else 
	echo "rootfs dir doesn't exist"
	exit 1
fi 


pushd "$_path_to_SD/boot/"
echo "rm boot files"
rm -r ./*
popd
pushd "$_path_to_SD/rootfs"
echo "rm rootfs files"
rm -r ./*
popd

pushd $_path_to_output 
cp MLO  "$_path_to_SD/boot"
cp u-boot.img  "$_path_to_SD/boot"
# cp zImage  "$_path_to_SD/boot"
cp uEnv.txt "$_path_to_SD/boot"
cp am335x-boneblack.dtb  "$_path_to_SD/boot"

tar -C "$_path_to_SD/rootfs" -xf "$_path_to_output/rootfs.tar"
popd

pushd $_path_to_kernels
cp "Server/zImage" "$_path_to_SD/rootfs/root/zImage_server"
cp "RT/zImage" "$_path_to_SD/rootfs/root/zImage_rt"
cp "Desktop/zImage" "$_path_to_SD/rootfs/root/zImage_desktop"
cp "BasicRT/zImage" "$_path_to_SD/rootfs/root/zImage_basicrt"
cp "LowLatencyDesktop/zImage" "$_path_to_SD/rootfs/root/zImage_lld"

cp "Server/zImage" "$_path_to_SD/boot"

chmod u+x start.sh
cp start.sh "$_path_to_SD/rootfs/root/"

popd

pushd $_path_to_TP2
make clean
make 

cp "question4c" "$_path_to_SD/rootfs/root/"
popd 

echo "umount boot"
umount "$_path_to_SD/boot"
echo "umount rootfs"
umount "$_path_to_SD/rootfs"

exit 0
