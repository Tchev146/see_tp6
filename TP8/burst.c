#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#define NB_THREADS 1000

void *function
(
    void *arg
)
{
    system("ping 172.18.3.10 -i 0");
    pthread_exit(0);
}

int main
(
    int argc, 
    char ** argv
)
{
    pthread_t threads[NB_THREADS];
    int retour_code = 0;
    int compteur_boucle = 0;

    for (compteur_boucle = 0; compteur_boucle < NB_THREADS; compteur_boucle++) 
    {
        retour_code = pthread_create(&threads[compteur_boucle], NULL, function, NULL);
        if (retour_code)
        {
            printf("ERROR; le code de retour de pthread_create() est %d\n", retour_code);
            exit(EXIT_FAILURE);
        }
    }

    for (compteur_boucle = 0; compteur_boucle < NB_THREADS; compteur_boucle++) 
    {
        retour_code = pthread_join(threads[compteur_boucle], NULL);
        if (retour_code)
        {
            printf("ERROR; le code de retour du pthread_join() est %d\n", retour_code);
            exit(EXIT_FAILURE);
        }
    }

    exit(EXIT_SUCCESS);
}
