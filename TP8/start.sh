#!/bin/bash

_boolean=1
_kernel_to_cp="void"
_current_kernel="void"

if [ "$#" -eq 1 ]
then
    _kernel_to_cp=$1
    echo "Kernel to copy : $_kernel_to_cp"
fi

if [ ! -e "./result.txt" ]
then 
    touch result.txt
fi

if [ ! -e "./current_kernel" ]
then 
    touch ./current_kernel
    echo "zImage_server" > ./current_kernel
fi
   
_current_kernel=`cat ./current_kernel`
echo "current kernel : $_current_kernel"

if [ $_boolean -eq 1 ]
then 
    ./question4d_perturb
fi

echo "######################################" >> result.txt
echo "########## $_current_kernel ##########" >> result.txt
echo "######################################" >> result.txt

./question4c >> result.txt
echo -e "\n"

if [ ! "$_kernel_to_cp" = "void" ]
then
    mkdir -p "/media/boot"
    mount "/dev/mmcblk0p1" "/media/boot"
    # rm -rf "/media/boot/zImage"
    cp "./$_kernel_to_cp" "/media/boot/zImage"

    echo "$_kernel_to_cp" > ./current_kernel

    sleep 5

    umount "/media/boot"

    reboot
fi

exit 0
